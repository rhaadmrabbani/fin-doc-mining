import os
import sys
from collections import defaultdict, Counter
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer



path_to_sec_form_parser_lib = '../lib'

companies_path = 'data/companies.csv'

word_list_path = 'data/WordList_Derivatives.txt'

interm_repr_dir = 'data/10-K_interm_repr'

tokenized_repr_dir = 'data/10-K_tokenized'

key_to_sort_filenames_by_cik = lambda name : int( name.split( '_' , 1 )[ 0 ] )



sys.path.append( path_to_sec_form_parser_lib )
from utils.utils import *
from utils.io_utils import *
from utils.text_utils import *



def get_word_list( ) :
    
    word_list = load_text( word_list_path )
    word_list = word_list.split( '\n' )
    word_list = [ word.lower( ).strip( ) for word in word_list ]
    word_list = [ word for word in word_list if word and word != 'rule names:' ]

    return word_list



def create_word_list_re( word_list ) :
    
    word_list_re = [ parens_re.sub( r'|\1' , word ).replace( ' ' , r'\s+' ) for word in word_list ]
    word_list_re = [ '(?P<group_' + str( i ) + '>' + word + ')' for i , word in enumerate( word_list_re ) ]
    word_list_str = '|'.join( word_list_re )
    word_list_re = re.compile( word_list_str )
    
    return word_list_re



parens_re = re.compile( r'\s*\((.*?)\)' )

tag_re = re.compile( r'<.*?>' , re.S )

table_re = re.compile( r'<table>.*?</table>', re.I | re.S )

token_re = re.compile( r'[a-z]+|[^a-z\s]+' , re.I )

word_re = re.compile( r'[a-z]+' , re.I )

filename_re = re.compile( '^(?P<cik>\d+)_(?P<yyyy>\d{4})-(?P<mm>\d{2})-' )



if __name__ == '__main__' :    
    
    
    headers , rows = load_csv( companies_path , headers = ['SIC', 'CIK', 'Company Name'] )
    ciks = [ ] ; sic_to_ciks = defaultdict( list ) ; cik_to_name = { }
    for row in rows :
        sic = int( row[ 0 ] ) ; cik = int( row[ 1 ] ) ; name = row[ 2 ]
        ciks.append( cik ) ; sic_to_ciks[ sic ].append( cik ) ; cik_to_name[ cik ] = name    
    
    
    filenames = sorted( os.listdir( interm_repr_dir ) , key = key_to_sort_filenames_by_cik )

    
    word_list = get_word_list( )    
    word_list_re = create_word_list_re( word_list )
    
    for filename in filenames :

        text = load_text( interm_repr_dir + '/' + filename )
        text = table_re.sub( '' , text )
        text = tag_re.sub( '' , text )
    
        tokens = [ m.group( ) for m in token_re.finditer( text ) ]
        text = ' '.join( tokens )
        
        word_count = len( list( word_re.finditer( text ) ) )
    
        words = [ ]
        for m in word_list_re.finditer( text ) :
            for i in range( len( word_list ) ) :
                if m.group( 'group_' + str( i ) ) :
                    word = word_list[ i ]
            words.append( word.replace( ' ' , '_' ) )
            word_count -= word.count( ' ' )
                    
        text = str( word_count ) + ' ' + ' '.join( words )
    
        save_text( tokenized_repr_dir + '/' + filename , text )        
        
    
    cik_year_to_word_count = Counter( )
    cik_year_to_text = defaultdict( str )
    years = set( )
    
    for filename in filenames :
        
        m = filename_re.match( filename )
        cik = int( m.group( 'cik' ) ) ; year = int( m.group( 'yyyy' ) ) ; month = int( m.group( 'mm' ) )
        if month <= 6 : year -= 1
        
        text = load_text( tokenized_repr_dir + '/' + filename )
        word_count , text = text.split( ' ' , 1 )
        word_count = int( word_count )
        
        cik_year_to_word_count[ ( cik , year ) ] += word_count
        cik_year_to_text[ ( cik , year ) ] += ' ' + text
        years.add( year )    
    
    years = sorted( years )
    
    
    corpus = [ ]
    word_counts = [ ]
    cik_year_to_row = { }
    
    for cik_year in sorted( cik_year_to_word_count ) :
        
        cik_year_to_row[ cik_year ] = len( corpus )
        corpus.append( cik_year_to_text[ cik_year ] )
        word_counts.append( [ float( cik_year_to_word_count[ cik_year ] ) ] )
    
    vectorizer = CountVectorizer( min_df = 1 )
    mat = vectorizer.fit_transform( corpus ) # each row represents a cik_year, each column repesents a word
    mat = np.asarray( mat.todense( ) )
    norm_mat = np.divide( mat , np.asarray( word_counts ) )
    words = [ str( word ) for word in vectorizer.get_feature_names( ) ]
    
    
    header = 'sic'.ljust( 8 ) + 'cik'.ljust( 8 ) + '  ' + '  '.join( [ str( year ) for year in years ] )
    
    for col , word in enumerate( words ) :
        
        print word
        print '-' * len( word )
        print header
        print
        for sic in sorted( sic_to_ciks ) :
            for cik in sic_to_ciks[ sic ] :
                s = ''.join( [ ( str( mat[ cik_year_to_row[ ( cik , year ) ] , col ] ) if ( cik , year ) in cik_year_to_row else '-' ).rjust( 6 )
                               for year in years ] )
                s = s.replace( ' 0' , ' -' )
                s = str( sic ).ljust( 8 ) + str( cik ).ljust( 8 ) + s + '  ' + cik_to_name[ cik ]
                print s
        print
        print header
        print
        for sic in sorted( sic_to_ciks ) :
            for cik in sic_to_ciks[ sic ] :
                s = ''.join( [ ( '{:.0e}'.format( norm_mat[ cik_year_to_row[ ( cik , year ) ] , col ] ).rjust( 6 ) if ( cik , year ) in cik_year_to_row else '-').rjust( 6 )
                               for year in years ] )
                s = s.replace( '0e+00' , '    -' ).replace( 'nan' , '  -' )
                s = str( sic ).ljust( 8 ) + str( cik ).ljust( 8 ) + s + '  ' + cik_to_name[ cik ]
                print s
        print
        print '-' * len( header )
        print
