import os
import re
import sys
from collections import defaultdict, OrderedDict
from difflib import Differ, SequenceMatcher
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
from wordcloud import WordCloud
from PIL import Image
import numpy as np
from scipy.interpolate import spline
import copy
import csv_utils

def main( ) :
    
    base_inpdir = '../10K sections'
    
    differ = Differ( )    
    
    cik_to_fy_dict = get_cik_to_fy_dict( )
    cik_to_inpdirs_dict = get_cik_to_inpdirs_dict( base_inpdir )
    cik_to_inpdirs_dict = filter_cik_to_inpdirs_dict( cik_to_inpdirs_dict , cik_to_fy_dict , base_inpdir )
    
    cik_to_year_to_sec1A_sentences = { }
    #cik_to_year_to_sec7_sentences = { }
    for cik , inpdirs in cik_to_inpdirs_dict.iteritems( ) :
        cik_to_year_to_sec1A_sentences[ cik ] = dict( [ ( inpdir_re.match( inpdir ).group( 'year' ) , get_sentences( base_inpdir + '/' + inpdir + '/1A.txt' ) ) for inpdir in inpdirs ] )
        
    v = []
    v1 = [] 
    v2 = []
    new_word_all = [] 
    stop_words = list(set(stopwords.words("english")))
    failed_cik = []
    
    #stop_words = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', ' the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should', 'now']
    stop_words.append(',')
    #print("stop_words", stop_words)
    
    #Changed sentences frequency dictionary
    sent_change_dict = OrderedDict([('2006', 0), ('2007', 0), ('2008', 0), ('2009', 0), ('2010', 0), ('2011', 0), ('2012', 0), ('2013', 0), ('2014', 0), ('2015', 0), ('2016', 0)])
    
    #Plot changed sentences frequency
    fig = plt.figure(figsize=(30, 15))
    ax = plt.subplot(111)
    a = np.arange(2006, 2017)

    for cik , year_to_sentences in cik_to_year_to_sec1A_sentences.items( ) :
        
            #fi.write(str(len(year_to_sentences)))
            #fi.write("\n")
            
            #print '{} : failed {}'.format( cik , cik_to_fy_dict[ cik ] )            
            years = sorted( year_to_sentences )
            
            first_failed_year = years[0]
            
            for y in range( len( years ) - 1 ) :
                
                sentences1 = year_to_sentences[ years[ y ] ]
                sentences2 = year_to_sentences[ years[ y + 1 ] ]  
                
                #print "HELLO SENTENCES"
                #print "sentences1" + str(sentences1)
                #print "sentences2" + str(sentences2)
     
                ratio_to_matches_dict = get_ratio_to_matches_dict( sentences1 , sentences2 , differ )     
                '''
                f.write("cik: " + str(cik))
                f.write("\n")
                f.write(years[y] + "\t" + years[y+1])
                f.write("\n")
                '''
                for ratio in sorted(ratio_to_matches_dict):
                    #f.write(str(ratio))
                    #f.write("\n")
                
                    matches = ratio_to_matches_dict[ratio]
                    #f.write(str(matches))
                    #f.write("\n")
                    #f.write("hi\n")
                    
                    for i in range(len(matches))[:-1:2]:
                        #f.write(matches[i])
                        #f.write("\n")
                        #f.write(str(i) + str(len(matches)))
                        #f.write("\n")
                        v = list(differ.compare(word_tokenize(matches[i]), word_tokenize(matches[i+1])))
                        #f.write("v: " + str(v))
                        #f.write("\n")
                        #print("stop_words", stop_words)
                        new_word = []
                        for word in v:
                            #f.write("word: " + str(word))
                            if word.startswith("+") == True:
                                if (word.split('+ ')[1]).lower() not in stop_words:
                                    new_word.append(word.split('+')[1])
         
                        #f.write("new_word_list: " + str(new_word))
                        #f.write("\n")
                        
                    '''    
                    for m in ratio_to_matches_dict[ratio]:
                        f.write(m)
                        f.write("\n")
                    '''
                    
                    #f.write("\n")
                    v1.extend(new_word)
                    
                v2.extend(v1)
                
                ratio_floors = sorted( ratio_to_matches_dict , reverse = True )
                
                
                ratio_to_num_sentences_dict = OrderedDict( [ ( ratio_floor , len( ratio_to_matches_dict[ ratio_floor ] ) ) for ratio_floor in ratio_floors ] )
                total_num_sentences = float( sum( ratio_to_num_sentences_dict.values( ) ) )
                ratio_to_num_sentences_percentage_dict = OrderedDict( [ ( ratio_floor , int( num_sentences / total_num_sentences * 100 ) ) for ratio_floor , num_sentences in ratio_to_num_sentences_dict.iteritems( ) ] )
                
                #print years[ y ] , years[ y + 1 ] , ':' , ' '.join( [ '{}:{}%'.format( '100' if ratio_floor == 100 else '90-<100' if ratio_floor == 90 else '80-<90' if ratio_floor == 80 else '70-<80' if ratio_floor == 70 else '0-<70',
                                                                                       #ratio_to_num_sentences_percentage_dict[ ratio_floor ] ).ljust( 12 )
                                                                      #for ratio_floor in [ 100 , 90 , 80 , 70 , 0 ] ] )
    
                #print( "---------------------------------------------------")
                #print("ratio_to_num_sentences_percentage_dict" + str(ratio_to_num_sentences_percentage_dict))
            
                if years[y] == first_failed_year:
                    sent_change_dict[str(years[y])] = 0
                    sent_change_dict[str(years[y+1])] = 100 - (np.sum(ratio_to_num_sentences_percentage_dict.values()) - ratio_to_num_sentences_percentage_dict[100])
                    
                    continue
                elif str(years[y+1]) in sent_change_dict:
                    sent_change_dict[str(years[y + 1])] = 100 - (np.sum(ratio_to_num_sentences_percentage_dict.values()) - ratio_to_num_sentences_percentage_dict[100])
            
            #print("sent_change_dict", sent_change_dict)  
            #print("\n")
            
            new_sent_change_dict = {}
            new_sent_change_dict = copy.deepcopy(sent_change_dict)

            for k,v in new_sent_change_dict.items():
                if v == 0 and k != first_failed_year:
                    del new_sent_change_dict[k]
                elif k < first_failed_year:
                    del new_sent_change_dict[k]
       
            print("CIK: ", cik, new_sent_change_dict)  
            print("\n")
            
            failed_cik.append(cik)
            #print(failed_cik)
            #print("\n")
            
            new_word_all.extend(v2)
            
            ha = new_sent_change_dict.keys()
            y1 = new_sent_change_dict.values()
            line = ax.plot(ha, y1, label = cik)
            
            plt.xticks(np.arange(min(a), max(a)+1, 1))
    
    #f.write("new_word_all" + str(new_word_all))
    #f.write("\n")
    
    #wordListTowordCountFreqDict(new_word_all)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])

# Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.07), fancybox=True, shadow=True, ncol=5, title = "FAILED BANK CIK")
    #plt.legend(flip(failed_cik,1), loc='best', title = "FAILED BANK CIK")
    plt.xlabel('Year')
    plt.ylabel('Fraction of Change Level (%)')
    plt.title('Fraction of changed sentences from one year to the next per bank')
    plt.show()
    
    
# Load boolean values for 'Negative' , 'Positive' , 'Uncertainty' and 'Litigious'
# for every word from the Loughran McDonald Master Dictionary.
# The stemmed version of each word is stored as a key. 

# Get dictionary mapping each failed bank's holding company's cik to its fail year.

def get_cik_to_fy_dict( ):
    
    cik_to_fy_dict = { }
    
    for cik , fy in csv_utils.csv_read( 'cik&fys.txt' , [ 'cik' , 'fy' ] ) :
        if fy != 'NA' :
            cik_to_fy_dict[ int( cik ) ] = fy
            
    #print "cik_to_fy_dict" + str(cik_to_fy_dict)

    #print("---------------------------------------------------")
    
    return cik_to_fy_dict


# Get dictionary mapping every cik to directories containing its 10K sections.

inpdir_re = re.compile( r'(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})-(?P<cik>\d+)' )

def get_cik_to_inpdirs_dict( base_inpdir ) :
   
    cik_to_inpdirs = defaultdict( list )    
    
    for inpdir in os.listdir( base_inpdir ) :
        
        m = inpdir_re.match( inpdir )
        year = m.group( 'year' )
        cik = int( m.group( 'cik' ) )
        
        if year < '2006' : continue
        cik_to_inpdirs[ cik ].append( inpdir )
        
    #print "cik_to_inpdirs" + str(cik_to_inpdirs)
     
    #print("---------------------------------------------------")
        
    return cik_to_inpdirs


# Filter away cik of non-failed banks and those with
# 4 or fewer 10K directories containing text from both 1A and 7.

def filter_cik_to_inpdirs_dict( cik_to_inpdirs_dict , cik_to_fy_dict , base_inpdir ) :
    
    for cik in sorted( cik_to_inpdirs_dict ) :
        
        if not cik in cik_to_fy_dict :
            del cik_to_inpdirs_dict[ cik ]
            continue
        
        inpdirs = [ ]
        for inpdir in cik_to_inpdirs_dict[ cik ] :
            sec1A_path = base_inpdir + '/' + inpdir + '/1A.txt'
            sec7_path = base_inpdir + '/' + inpdir + '/7.txt'
            if os.path.isfile( sec1A_path ) and os.path.getsize( sec1A_path ) > 500 and os.path.isfile( sec7_path ) and os.path.getsize( sec7_path ) > 5000 :
                inpdirs.append( inpdir )
        
        if len( inpdirs ) > 3 :
            cik_to_inpdirs_dict[ cik ] = inpdirs
        else :
            del cik_to_inpdirs_dict[ cik ]
            
    #print "cik_to_inpdirs_dict" + str(cik_to_inpdirs_dict)
    #print("---------------------------------------------------")
                    
    return cik_to_inpdirs_dict
            

# Extract sentences from text file

tag_re = re.compile( r'<.*?>' )

page_sep_re = re.compile( r'(^|\n\s*\n)?(page [^\n]*|-? *\d+ *-?)(\n\s*\ntable of contents?)?(\n\s*\n|$)' , re.I )
bad_page_end_re = re.compile( r'[\w,]$' )
bad_page_start_re = re.compile( r'^[a-z]' )

para_sep_re = re.compile( r'\n\s*\n|\.  ' , re.I )
sentence_sep_re = re.compile( r'( [^A-Z][\S]*)\.(\s*$| *[A-Z][a-z]*[, ])' )

def get_sentences( inp_path ) :
    
    inp = open( inp_path )
    text = inp.read( )
    inp.close( )    
    
    # clean up
    text = tag_re.sub( '' , text )
    
    pages = page_sep_re.split( text )[ : : 5 ]

    # mend paras split across page separations
    i = 0
    while i < len( pages ) - 1 :
        if bad_page_end_re.search( pages[ i ] ) and bad_page_start_re.search( pages[ i + 1 ] ) :
            pages[ i : i + 2 ] = [ pages[ i ] + ' ' + pages[ i + 1 ] ]
            continue
        i += 1  
    
    text = '\n\n'.join( pages )
    paras = [ para.strip( ) for para in para_sep_re.split( text ) if para.strip( ) ]
    
    sentences = [ ]
    for para in paras :
        para_sentences = sentence_sep_re.split( para )
        para_sentences = [ ' '.join( [ sentence.strip( ) for sentence in para_sentences[ : 2 ] ] ) + '.' ] \
            + [ ' '.join( [ sentence.strip( ) for sentence in para_sentences[ j : j + 3 ] ] ) + '.' for j in range( len( para_sentences ) )[ 2 : -2 : 3 ] ]
        sentences += para_sentences
    #print "SENTENCES" + str(sentences)
    #print "input path" + str(inp_path)
 
    #print("---------------------------------------------------")
    return sentences


def get_ratio_to_matches_dict( sentences1 , sentences2 , differ ) :
    
    ratio_to_matches_dict = defaultdict( list )
    for ratio_floor in [ 100 , 90 , 80 , 70 , 0 ] :
        ratio_to_matches_dict[ ratio_floor ]
    
    while True :
    
        num_sentences1 = len( sentences1 )
        num_sentences2 = len( sentences2 )   
        
        diff = list( differ.compare( sentences1 , sentences2 ) )
        
        i = 0
        
        while i < len( diff ) :
            
            if diff[ i ].startswith( '  ' ) :            
                # diff[ i ] is unchanged sentence
                ratio_to_matches_dict[ 100 ] += [ diff[ i ][ 2 : ] ] * 2
                # remove diff[ i ]
                diff[ i : i + 1 ] = [ ]
                continue
            
            elif diff[ i ].startswith( '? ' ) :
                
                if diff[ i - 1 ].startswith( '+ ' ) :
                    # - diff[ i - 2 ], + diff[ i - 1 ], ? diff[ i ]
                    # diff[ i - 1 ] == diff[ i - 2 ] + additions
                    ratio = SequenceMatcher( None , diff[ i - 2 ][ 2 : ] , diff[ i - 1 ][ 2 : ] ).ratio( )
                    ratio = int( ratio * 10 ) * 10
                    ratio_to_matches_dict[ ratio ] += [ diff[ i - 2 ][ 2 : ] , diff[ i - 1 ][ 2 : ] ]
                    # remove diff[ i - 2 ] to [ i ]
                    diff[ i - 2 : i + 1 ] = [ ]
                    i = i - 2
                    
                else :
                    # - diff[ i - 1 ] , ? diff[ i ], + diff[ i + 1 ] , ? diff[ i + 2 ]
                    ratio = SequenceMatcher( None , diff[ i - 1 ][ 2 : ] , diff[ i + 1 ][ 2 : ] ).ratio( )
                    ratio = int( ratio * 10 ) * 10
                    ratio_to_matches_dict[ ratio ] += [ diff[ i - 1 ][ 2 : ] , diff[ i + 1 ][ 2 : ] ]
                    #remove diff[ i - 1 ] to [ i + 2 ] if '? diff[ i + 2 ]' else [ i + i ]
                    diff[ i - 1 : i + 3 if ( i + 2 < len( diff ) and diff[ i + 2 ].startswith( '? ' ) ) else i + 2 ] = [ ]
                    i = i - 1
                    
                continue
            
            i += 1
        
        # split remaining diff results to sentences1 and sentences2
        sentences1 = [ sentence[ 2 : ] for sentence in diff if sentence.startswith( '- ' ) ]
        sentences2 = [ sentence[ 2 : ] for sentence in diff if sentence.startswith( '+ ' ) ]
        
        if len( sentences1 ) == num_sentences1 and len( sentences2 ) == num_sentences2 :
            break

    ratio_to_matches_dict[ 0 ] = diff
    #print "RATIO" + str(ratio_to_matches_dict)
    
    #print("---------------------------------------------------")
    return ratio_to_matches_dict

def wordListTowordCountFreqDict(wordlist):
    wordfreq = [wordlist.count(p) for p in wordlist]
    freqdict = dict(zip(wordlist,wordfreq))
    
    
    count_freq = [(freqdict[key], key) for key in freqdict]
    count_freq.sort()
    count_freq.reverse()
    print("Word Frequency Dictionary")
    print(count_freq)
    
    
    MyData = dict(freqdict)
    wc = WordCloud().generate_from_frequencies(MyData)
    
    plt.figure(figsize=(12,4))
    plt.subplot(1,2,1)
    plt.imshow(wc)

if __name__ == '__main__' :
    main( )
    
#sys.stdout.close()