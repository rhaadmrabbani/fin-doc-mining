import os
import os.path
import re
from collections import defaultdict
from alignment.sequence import Sequence
from alignment.vocabulary import Vocabulary
from alignment.sequencealigner import SimpleScoring , GlobalSequenceAligner , LocalSequenceAligner


inp_path = '../py13/cik&fys.txt'

inp = open( inp_path )
lines = inp.readlines( )
inp.close( )

cik_to_fy = { }

for line in lines :
    line = line.split( ',' )
    cik = line[ 0 ].strip( )
    fy = line[ 1 ].strip( )
    if fy != 'NA' :
        cik_to_fy[ cik ] = fy


base_inp_dir = '../py13/sections/10-K'

inp_dir_re = re.compile( r'(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})-(?P<cik>\d+)' )

cik_to_inp_dirs = defaultdict( list )

for inp_dir in os.listdir( base_inp_dir ) :
    inp_dir_m = inp_dir_re.match( inp_dir )
    cik = inp_dir_m.group( 'cik' )
    if cik in cik_to_fy :
        cik_to_inp_dirs[ cik ].append( inp_dir )

for cik , inp_dirs in cik_to_inp_dirs.items( ) :
    inp_dirs_1A = [ inp_dir for inp_dir in inp_dirs if os.path.isfile( base_inp_dir + '/' + inp_dir + '/1A.txt' ) and os.path.getsize( base_inp_dir + '/' + inp_dir + '/1A.txt' ) > 500 ]
    inp_dirs_7 = [ inp_dir for inp_dir in inp_dirs if os.path.isfile( base_inp_dir + '/' + inp_dir + '/7.txt' ) and os.path.getsize( base_inp_dir + '/' + inp_dir + '/7.txt' ) > 5000 ]
    inp_dirs = sorted( set( inp_dirs_1A ) & set( inp_dirs_7 ) )
    if len( inp_dirs ) > 4 :
        cik_to_inp_dirs[ cik ] = inp_dirs
    else :
        del cik_to_inp_dirs[ cik ]


def get_alignments( text1 , text2 ) :
    v = Vocabulary()
    text1_encoded = v.encodeSequence( Sequence( text1.lower( ).split( ) ) )
    text2_encoded = v.encodeSequence( Sequence( text2.lower( ).split( ) ) )
    scoring = SimpleScoring( 2 , -1 )
    aligner = GlobalSequenceAligner( scoring , -1 )
    score, encodeds = aligner.align( text1_encoded, text2_encoded, backtrace=True)
    alignments = [ v.decodeSequenceAlignment( encoded ) for encoded in encodeds ]
    return alignments        


para_sep_re = re.compile( r'\n\s*\n((page [^\n]*|\d+)\n\s*\n)?|\d+\n\s*\n|\n\s*\n|\.  ' , re.I )
para_sep_re2 = re.compile( r'\. ?(?=(Accordingly|Actions|Any|As|Because|Changes|Consequently|Expansion|Failure|Future|However|If|In|Investors|Our|Management|Many|Market|Material|Revently|The|This|We|With)[, ])' )
tag_re = re.compile( r'<.*?>' )

def find_para1_in_paras2( para1 , paras1 , paras2 , good_matches , bad_matches ) :
    for para2 in paras2 :
        for alignment in get_alignments( para1 , para2 ) :
            if alignment.percentIdentity( ) == 100 and para1.lower( ) == para2.lower( ) :
                paras1.remove( para1 )
                paras2.remove( para2 )
                return
            elif alignment.percentIdentity( ) > 70 and alignment.score > 20 :
                len1 = float( len( para1 ) )
                len2 = float( len( para2 ) )
                if min( len1 , len2 ) / max( len1 , len2 )  > .8 :
                    good_matches.append( ( para1 , para2 ) )
                else :
                    bad_matches.append( ( para1 , para2 ) )
                paras1.remove( para1 )
                paras2.remove( para2 )                
                return


for cik , inp_dirs in cik_to_inp_dirs.items( )[ 7 : 8 ] :
    for i in range( len( inp_dirs ) - 1 )[ 0 : 1 ] :
        inp1_path = base_inp_dir + '/' + inp_dirs[ i ] + '/1A.txt'
        inp2_path = base_inp_dir + '/' + inp_dirs[ i + 1 ] + '/1A.txt'
        print inp1_path , inp2_path
        inp1 = open( inp1_path )
        text1 = inp1.read( )
        inp1.close( )
        inp2 = open( inp2_path )
        text2 = inp2.read( )
        inp2.close( )
        text1 = tag_re.sub( '' , text1 )
        text2 = tag_re.sub( '' , text2 )
        paras1 = [ para.strip( ) for para in para_sep_re.split( text1 )[ : : 3 ] if para.strip( ) ]
        paras2 = [ para.strip( ) for para in para_sep_re.split( text2 )[ : : 3 ] if para.strip( ) ]
        paras1 = [ para if para.endswith( '.' ) else para + '.' for para in paras1 ]
        paras2 = [ para if para.endswith( '.' ) else para + '.' for para in paras2 ]
        good_matches = [ ]
        bad_matches = [ ]
        for para1 in paras1[ : ] :
            find_para1_in_paras2( para1 , paras1 , paras2 , good_matches , bad_matches )
        for para1 , para2 in good_matches :
            print '-' * 40
            print para1
            print
            print para2
            print
        for para1 , para2 in bad_matches :
            print '/' * 40
            print para1
            print
            print para2
            print        
        print '*' * 40
        for para1 in paras1 :
            print para1
            print
        print '*' * 40
        for para2 in paras2 :
            print para2
            print
        
            
                    