import os
import re
from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
#from nltk.stem.porter import PorterStemmer
from difflib import Differ, SequenceMatcher

import csv_utils


def main( ) :
    
    base_inpdir = '../10K sections'
    
    #stemmer = PorterStemmer( )
    differ = Differ( )    
    
    #LM_dict = get_LM_dict( stemmer )
    cik_to_fy_dict = get_cik_to_fy_dict( )
    cik_to_inpdirs_dict = get_cik_to_inpdirs_dict( base_inpdir )
    cik_to_inpdirs_dict = filter_cik_to_inpdirs_dict( cik_to_inpdirs_dict , cik_to_fy_dict , base_inpdir )
    
    cik_to_year_to_sec1A_sentences = { }
    cik_to_year_to_sec7_sentences = { }
    for cik , inpdirs in cik_to_inpdirs_dict.iteritems( ) :
        cik_to_year_to_sec1A_sentences[ cik ] = dict( [ ( inpdir_re.match( inpdir ).group( 'year' ) , get_sentences( base_inpdir + '/' + inpdir + '/1A.txt' ) ) for inpdir in inpdirs ] )
        # cik_to_year_to_sec7_sentences[ cik ] = dict( [ ( inpdir_re.match( inpdir ).group( 'year' ) , get_sentences( base_inpdir + '/' + inpdir + '/7.txt' ) ) for inpdir in inpdirs ] )
        
    for cik , year_to_sentences in cik_to_year_to_sec1A_sentences.items( ) :
            
            print '{} : failed {}'.format( cik , cik_to_fy_dict[ cik ] )            
            years = sorted( year_to_sentences )
            
            for y in range( len( years ) - 1 ) :
                
                sentences1 = year_to_sentences[ years[ y ] ]
                sentences2 = year_to_sentences[ years[ y + 1 ] ]                
                ratio_to_matches_dict = get_ratio_to_matches_dict( sentences1 , sentences2 , differ )                
                ratio_floors = sorted( ratio_to_matches_dict , reverse = True )
                
                ratio_to_num_sentences_dict = dict( [ ( ratio_floor , len( ratio_to_matches_dict[ ratio_floor ] ) ) for ratio_floor in ratio_floors ] )
                total_num_sentences = float( sum( ratio_to_num_sentences_dict.values( ) ) )
                ratio_to_num_sentences_percentage_dict = dict( [ ( ratio_floor , int( num_sentences / total_num_sentences * 100 ) ) for ratio_floor , num_sentences in ratio_to_num_sentences_dict.iteritems( ) ] )
                
                print years[ y ] , years[ y + 1 ] , ':' , ' '.join( [ '{}:{}%'.format( '100' if ratio_floor == 100 else '90-<100' if ratio_floor == 90 else '80-<90' if ratio_floor == 80 else '70-<80' if ratio_floor == 70 else '0-<70',
                                                                                       ratio_to_num_sentences_percentage_dict[ ratio_floor ] ).ljust( 12 )
                                                                      for ratio_floor in [ 100 , 90 , 80 , 70 , 0 ] ] )
            

# Load boolean values for 'Negative' , 'Positive' , 'Uncertainty' and 'Litigious'
# for every word from the Loughran McDonald Master Dictionary.
# The stemmed version of each word is stored as a key. 

def get_LM_dict( stemmer ) :

    csv_path = 'LoughranMcDonald_MasterDictionary_2016.csv'
    headers = [ 'Word' , 'Negative' , 'Positive' , 'Uncertainty' , 'Litigious' ]
    
    LM_dict = { }
    
    for word , neg , pos , unc , lit in csv_utils.csv_read( csv_path , headers = headers ) :
        if not neg == pos == unc == lit == '0' :
            word = stemmer.stem( word.lower( ) )
            values = [ neg , pos , unc ,lit ]
            values = [ ( b != '0' ) * 1 for b in values ]
            LM_dict[ word ] = values
    
    return LM_dict


# Get dictionary mapping each failed bank's holding company's cik to its fail year.

def get_cik_to_fy_dict( ) :
    
    cik_to_fy_dict = { }
    
    for cik , fy in csv_utils.csv_read( 'cik&fys.txt' , [ 'cik' , 'fy' ] ) :
        if fy != 'NA' :
            cik_to_fy_dict[ int( cik ) ] = fy
    
    return cik_to_fy_dict


# Get dictionary mapping every cik to directories containing its 10K sections.

inpdir_re = re.compile( r'(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})-(?P<cik>\d+)' )

def get_cik_to_inpdirs_dict( base_inpdir ) :
   
    cik_to_inpdirs = defaultdict( list )    
    
    for inpdir in os.listdir( base_inpdir ) :
        
        m = inpdir_re.match( inpdir )
        year = m.group( 'year' )
        cik = int( m.group( 'cik' ) )
        
        if year < '2006' : continue
        cik_to_inpdirs[ cik ].append( inpdir )
        
    return cik_to_inpdirs


# Filter away cik of non-failed banks and those with
# 4 or fewer 10K directories containing text from both 1A and 7.

def filter_cik_to_inpdirs_dict( cik_to_inpdirs_dict , cik_to_fy_dict , base_inpdir ) :
    
    for cik in sorted( cik_to_inpdirs_dict ) :
        
        if not cik in cik_to_fy_dict :
            del cik_to_inpdirs_dict[ cik ]
            continue
        
        inpdirs = [ ]
        for inpdir in cik_to_inpdirs_dict[ cik ] :
            sec1A_path = base_inpdir + '/' + inpdir + '/1A.txt'
            sec7_path = base_inpdir + '/' + inpdir + '/7.txt'
            if os.path.isfile( sec1A_path ) and os.path.getsize( sec1A_path ) > 500 and os.path.isfile( sec7_path ) and os.path.getsize( sec7_path ) > 5000 :
                inpdirs.append( inpdir )
        
        if len( inpdirs ) > 3 :
            cik_to_inpdirs_dict[ cik ] = inpdirs
        else :
            del cik_to_inpdirs_dict[ cik ]
                    
    return cik_to_inpdirs_dict
            

# Extract sentences from text file

tag_re = re.compile( r'<.*?>' )

page_sep_re = re.compile( r'(^|\n\s*\n)?(page [^\n]*|-? *\d+ *-?)(\n\s*\ntable of contents?)?(\n\s*\n|$)' , re.I )
bad_page_end_re = re.compile( r'[\w,]$' )
bad_page_start_re = re.compile( r'^[a-z]' )

para_sep_re = re.compile( r'\n\s*\n|\.  ' , re.I )
sentence_sep_re = re.compile( r'( [a-z0-9]*)\.(\s*$| *[A-Z][a-z]*[, ])' )

def get_sentences( inp_path ) :
    
    inp = open( inp_path )
    text = inp.read( )
    inp.close( )    
    
    # clean up
    text = tag_re.sub( '' , text )
    
    pages = page_sep_re.split( text )[ : : 5 ]

    # mend paras split across page separations
    i = 0
    while i < len( pages ) - 1 :
        if bad_page_end_re.search( pages[ i ] ) and bad_page_start_re.search( pages[ i + 1 ] ) :
            pages[ i : i + 2 ] = [ pages[ i ] + ' ' + pages[ i + 1 ] ]
            continue
        i += 1  
    
    text = '\n\n'.join( pages )
    paras = [ para.strip( ) for para in para_sep_re.split( text ) if para.strip( ) ]
    
    sentences = [ ]
    for para in paras :
        para_sentences = sentence_sep_re.split( para )
        para_sentences = [ ' '.join( [ sentence.strip( ) for sentence in para_sentences[ : 2 ] ] ) + '.' ] \
            + [ ' '.join( [ sentence.strip( ) for sentence in para_sentences[ i : i + 3 ] ] ) + '.' for i in range( len( para_sentences ) )[ 2 : -2 : 3 ] ]
        sentences += para_sentences
    
    return sentences


def get_ratio_to_matches_dict( sentences1 , sentences2 , differ ) :
    
    ratio_to_matches_dict = defaultdict( list )
    for ratio_floor in [ 100 , 90 , 80 , 70 , 0 ] :
        ratio_to_matches_dict[ ratio_floor ]
    
    while True :
    
        num_sentences1 = len( sentences1 )
        num_sentences2 = len( sentences2 )   
        
        diff = list( differ.compare( sentences1 , sentences2 ) )
        
        i = 0
        
        while i < len( diff ) :
            
            if diff[ i ].startswith( '  ' ) :            
                # diff[ i ] is unchanged sentence
                ratio_to_matches_dict[ 100 ] += [ diff[ i ][ 2 : ] ] * 2
                # remove diff[ i ]
                diff[ i : i + 1 ] = [ ]
                continue
            
            elif diff[ i ].startswith( '? ' ) :
                
                if diff[ i - 1 ].startswith( '+ ' ) :
                    # - diff[ i - 2 ], + diff[ i - 1 ], ? diff[ i ]
                    # diff[ i - 1 ] == diff[ i - 2 ] + additions
                    ratio = SequenceMatcher( None , diff[ i - 2 ][ 2 : ] , diff[ i - 1 ][ 2 : ] ).ratio( )
                    ratio = int( ratio * 10 ) * 10
                    ratio_to_matches_dict[ ratio ] += [ diff[ i - 2 ][ 2 : ] , diff[ i - 1 ][ 2 : ] ]
                    # remove diff[ i - 2 ] to [ i ]
                    diff[ i - 2 : i + 1 ] = [ ]
                    i = i - 2
                    
                else :
                    # - diff[ i - 1 ] , ? diff[ i ], + diff[ i + 1 ] , ? diff[ i + 2 ]
                    ratio = SequenceMatcher( None , diff[ i - 1 ][ 2 : ] , diff[ i + 1 ][ 2 : ] ).ratio( )
                    ratio = int( ratio * 10 ) * 10
                    ratio_to_matches_dict[ ratio ] += [ diff[ i - 1 ][ 2 : ] , diff[ i + 1 ][ 2 : ] ]
                    #remove diff[ i - 1 ] to [ i + 2 ] if '? diff[ i + 2 ]' else [ i + i ]
                    diff[ i - 1 : i + 3 if ( i + 2 < len( diff ) and diff[ i + 2 ].startswith( '? ' ) ) else i + 2 ] = [ ]
                    i = i - 1
                    
                continue
            
            i += 1
        
        # split remaining diff results to sentences1 and sentences2
        sentences1 = [ sentence[ 2 : ] for sentence in diff if sentence.startswith( '- ' ) ]
        sentences2 = [ sentence[ 2 : ] for sentence in diff if sentence.startswith( '+ ' ) ]
        
        if len( sentences1 ) == num_sentences1 and len( sentences2 ) == num_sentences2 :
            break

    ratio_to_matches_dict[ 0 ] = diff
    
    return ratio_to_matches_dict


if __name__ == '__main__' :
    main( )
    
'''

    
    


for cik , year_to_paras in cik_to_year_to_paras_1A.items( )[ 0 : 5 ] :
    
    print cik
    #print '{} : failed {}'.format( cik , cik_to_fy[ cik ] )
    
    years = sorted( year_to_paras )
    
    for y in range( len( years ) - 1 )[ 0 : ] :
        
        paras1 = year_to_paras[ years[ y ] ]
        paras2 = year_to_paras[ years[ y + 1 ] ]
        
        matches_by_pc = get_matches_by_pc( paras1 , paras2 )
        
        pcs = sorted( matches_by_pc , reverse = True )
        
        num_paras_list = [ len( matches_by_pc[ pc ] ) * ( 2 if pc == 100 else 1 ) for pc in pcs ]
        total_num_paras = float( sum( num_paras_list ) )
        num_paras_pc_list = [ int( num_paras / total_num_paras * 100 ) for num_paras in num_paras_list ]
        
        unchanged_sent_counts_list = [ np.sum( [ mast_dict[ word ]
                                           for word in 
                                           [ stemmer.stem( word.lower( ) ) for para in matches_by_pc[ pc ] for word in para.split( ) ] 
                                           if word in mast_dict ] ,
                                         axis = 0 ) / float( sum( [ 1 for para in matches_by_pc[ pc ] for word in para.split( ) ] ) )
                                 if matches_by_pc[ pc ] else np.zeros( 4 )
                                 for pc in pcs[ : 1 ] ]
        unchanged_sent_counts_list = [ ( sent_counts * 100 ).astype( int ).tolist( ) for sent_counts in unchanged_sent_counts_list ]        
        
        old_sent_counts_list = [ np.sum( [ mast_dict[ word ]
                                           for word in 
                                           [ stemmer.stem( word.lower( ) ) for para in matches_by_pc[ pc ] if para.startswith( '- ' ) for word in para.split( ) ] 
                                           if word in mast_dict ] ,
                                         axis = 0 ) / float( sum( [ 1 for para in matches_by_pc[ pc ] if para.startswith( '- ' ) for word in para.split( ) ] ) )
                                 if matches_by_pc[ pc ] else np.zeros( 4 )
                                 for pc in pcs[ 1 : ] ]
        old_sent_counts_list = [ ( sent_counts * 100 ).astype( int ).tolist( ) for sent_counts in old_sent_counts_list ]
        
        new_sent_counts_list = [ np.sum( [ mast_dict[ word ]
                                           for word in 
                                           [ stemmer.stem( word.lower( ) ) for para in matches_by_pc[ pc ] if para.startswith( '+ ' ) for word in para.split( ) ] 
                                           if word in mast_dict ] ,
                                         axis = 0 ) / float( sum( [ 1 for para in matches_by_pc[ pc ] if para.startswith( '+ ' ) for word in para.split( ) ] ) )
                                 if matches_by_pc[ pc ] else np.zeros( 4 )
                                 for pc in pcs ]
        new_sent_counts_list = [ ( sent_counts * 100 ).astype( int ).tolist( ) for sent_counts in new_sent_counts_list[ 1 : ] ]        
        
        print years[ y ] , years[ y + 1 ] , ':' , zip( pcs , num_paras_pc_list , unchanged_sent_counts_list + old_sent_counts_list , unchanged_sent_counts_list + new_sent_counts_list )
    
    print

    for pc in sorted( matches_by_pc , reverse = True ) :
        print pc
        for x in matches_by_pc[ pc ] : print x
        print
    '''
